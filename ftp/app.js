var count=0;
var to_email=[]
var prev="";

var app = angular.module('app', ['flow','ui.bootstrap'])
.config(['flowFactoryProvider', function (flowFactoryProvider) {
  flowFactoryProvider.defaults = {
    target: 'http://192.168.0.127:8000/ftp/upload/agridemo',
    testChunks:false,
    permanentErrors: [404, 500, 501],
    maxChunkRetries: 3,
    chunkRetryInterval: 5000,
    simultaneousUploads: 1,
    	chunkSize : 20000000,
  };

}]);
const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
app.controller('myController', function($scope,$http) {
      $scope.obj = {};
      $scope.progresswidth=0;
      $scope.selcetedfilter="all";
      $scope.viewby = 10;
      $scope.totalItems = 0;
      $scope.currentPage = 1;
      $scope.itemsPerPage = 50;
      $scope.numfiles=0
      $scope.maxSize = 5; //Number of pager buttons to show
      $scope.fileNames={}
      $scope.status="0 out of 0"
      $scope.individualWidth=0;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.directories_count={}
  $scope.addedFile=function($file, $event, $flow){
                $scope.totalItems=$scope.totalItems+1
                $scope.fileNames[$file.relativePath]=$file.name;
                var folder_path=$file.relativePath.split("/")
                if(folder_path.length > 1){
                folder_path.pop()
                if(isNaN($scope.directories_count[folder_path[0]+"/"+folder_path[1]])){
                    $scope.directories_count[folder_path[0]+"/"+folder_path[1]]=1;
                }
                else{
                $scope.directories_count[folder_path[0]+"/"+folder_path[1]]=$scope.directories_count[folder_path[0]+"/"+folder_path[1]]+1
                }
                }
                else{
                if(isNaN($scope.directories_count["rootFolder"]))
                    $scope.directories_count["rootFolder"]=1
                else
                    $scope.directories_count["rootFolder"]=$scope.directories_count["rootFolder"]+1
                }
  }
$scope.getprogresswidth=function(){
return Math.round($scope.progresswidth*100);
}

var getEmail=function(){
 $http({
        url: 'http://192.168.0.127:8000/ftp/email/',
        method: "GET",
    })
    .then(function(response) {

            to_email=response.data
            console.log(to_email)
            return to_email
    },
    function(response) { 
            console.log(response.text)
            return to_email
    });

}

getEmail()

var sendEmail=function(data){
console.log(data)
$http({
url: 'http://192.168.0.127:8000/ftp/users/send/',
method: "POST",
headers: {'Content-Type': 'application/json','Accept':'applicarion/json',"Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',},
data:  data
})
.then(function(response) {
console.log(response.data)
},
function(response) {
console.log(response.text)
});


}


$scope.filesubmit=function( $files, $event, $flow ){
console.log($scope.directories_count);
$http({
url: '/ftp/store/',
method: "POST",
headers: {'Content-Type': 'application/json','Accept':'applicarion/json'},
data:  {"project":proj,"total":$scope.totalItems,"allfiles":$scope.fileNames}
})
.then(function(response) {
console.log(response.data)
},
function(response) {
console.log(response.text)
});


console.log(to_email)
var data={
"action":"admin",
"Upload Status":"File upload initiated",
"to":to_email,
"title":"File Upload Initiation",
'uploader': user,
'Time of Upload Initiation ': new Date(),
"Number of files to be uploaded ":$scope.totalItems
}
sendEmail(data);
document.getElementById("over").style.display="block";


}
$scope.allFiles={}
    $scope.allFiles={}
    $scope.files=function( $file, $message, $flow ){
            $scope.numfiles=$scope.numfiles+1
            $scope.allFiles[$file.relativePath]=$file.name;
            console.log($scope.allFiles)
            now=$file.relativePath.split("/")
            now.pop()
            now=now.join("/")
            console.log(prev,now)
            if(prev==""){
            prev=now;
            }
            if(now != prev){
            console.log("folder changed ",now,prev);
             $http({
                method : "POST",
                  url : "http://192.168.0.127:8000/ftp/folder/agridemo",
                  data : {directories_count:$scope.directories_count}
              }).then(function mySuccess(response) {
                console.log("got response",response.data,prev,now);
                prev=now;
              }, function myError(response) {
              console.log("error response");
              });
              }
              else{
              console.log(" prev and now are same ",prev,now)
              }


      }


$scope.fileprogress=function( $file, $flow ){
$scope.individualWidth=$file.progress();
}


$scope.cancelAll=function($flow){
$flow.cancel();


setTimeout(function(){

var data={
"action":"admin",
"Upload Status":"File upload Cancelled by Uploader",
"to":to_email,
"title":"File Upload Cancellation"
}

sendEmail(data);
console.log(to_email)

},3000);

document.getElementById("over").style.display="none";
}

$scope.formatBytes=function(bytes) {
if ( ( bytes >> 30 ) & 0x3FF )
bytes = ( bytes >>> 30 ) + '.' + ( bytes & (3*0x3FF )) + 'GB' ;
else if ( ( bytes >> 20 ) & 0x3FF )
bytes = ( bytes >>> 20 ) + '.' + ( bytes & (2*0x3FF ) ) + 'MB' ;
else if ( ( bytes >> 10 ) & 0x3FF )
bytes = ( bytes >>> 10 ) + '.' + ( bytes & (0x3FF ) ) + 'KB' ;
else if ( ( bytes >> 1 ) & 0x3FF )
bytes = ( bytes >>> 1 ) + 'Bytes' ;
else
bytes = bytes + 'Byte' ;
return bytes ;
};
$scope.fileList=function(item){
switch($scope.selcetedfilter){
case "all": return true;break;
case "success":  return item.isComplete()?  ! item.error :  item.error;break;
case "fail": return item.error;break;
default: return true;
}
}
$scope.completedUploading=function(root,isFolder,files){
document.getElementById("over").style.display="none";
$http({
method : "POST",
url : "http://192.168.0.127:8000/ftp/success/agridemo",
headers:{"Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1'},
data : {root :root,isFolder:isFolder,files:files,allFiles:$scope.allFiles,user:localStorage.getItem('user'),directories_count:$scope.directories_count}
}).then(function mySuccess(response) {
var status = response.data;
if (status['status']=="success"){
toastr.success('Successfully uploaded and email is sent !')
}
else{
toastr.error("Error uploading file, Please try again")
}
}, function myError(response) {
console.log(response)
toastr.error("Error uploading file, Please try again")

});
}

$scope.$on('flow::filesAdded',function(event, flow, files){
console.log(files)
console.log(event)
console.log(flow)
data=[]
for(var i in files)
{
data.push({path:files[i].relativePath,file:files[i].file.name,size:files[i].size})

}

count=0;
$http({
url: 'http://192.168.0.127:8000/ftp/check/agridemo',
method: "POST",
headers: {'Content-Type': 'application/json','Accept':'applicarion/json'},
data:  data
})
.then(function(response) {
console.log(response.data)
for(var i in response.data){
if(! response.data[i].status){
flow.removeFile(files[i])
console.log(i)
}
else{
count++;
console.log("else",i)
}
}
console.log(count)
var temp=$scope.totalItems
$scope.totalItems=count
if(count==0){
alert("All files are already uploaded")
}
else{
alert(temp-count +" Number of Files are already present ")
}
},
function(response) {
console.log(response.text)
});


});

$scope.$on('flow::complete', function (event, $flow, flowFile) {
$scope.progresswidth=1;
$scope.status=$scope.numfiles+" out of "+$flow.files.length;
document.getElementById("numbers").innerHTML=$scope.numfiles+" out of "+$flow.files.length;
bar.animate($scope.progresswidth);
var root=$flow.opts.query.flowRelativePath.split("/")
if (root.length>1)
{
$scope.completedUploading(root[0],true,$flow.files.length)
}
else{
$scope.completedUploading(root,false,$flow.files.length)
}
});
$scope.$on('flow::progress', function (event, $flow, flowFile) {
$scope.progresswidth=$flow.files.length==1? $flow.progress(): $scope.numfiles/$flow.files.length;
console.log($scope.numfiles,$flow.files.length);
$scope.status=$scope.numfiles+" out of "+$flow.files.length;
var elem=document.getElementById("myB");
elem.style.width = $scope.individualWidth * 100+ '%';
document.getElementById("numbers").innerHTML="Current File : "+$flow.opts.query.flowFilename+" <br>Upload Progress "+$scope.numfiles+" out of "+$flow.files.length;
bar.animate($scope.progresswidth);
console.log($flow)
});


    window.addEventListener('offline', function(event){
       console.log("You lost connection.");
        $scope.obj.flow.pause();

    });
    window.addEventListener('online', function(event){
        console.log("You are now back online.");
        $scope.obj.flow.resume();
    });

});

