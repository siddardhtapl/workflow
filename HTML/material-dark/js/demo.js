 //[Preview Menu Javascript]

//Project:	Superieur Admin - Responsive Admin Template
//Primary use:   This file is for demo purposes only.

$(function () {
  'use strict'

  /**
   * Get access to plugins
   */

  $('[data-toggle="control-sidebar"]').controlSidebar()
  $('[data-toggle="push-menu"]').pushMenu()

  var $pushMenu       = $('[data-toggle="push-menu"]').data('lte.pushmenu')
  var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar')
  var $layout         = $('body').data('lte.layout')

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var mySkins = [
    'skin-info',
    'skin-black',
    'skin-danger',
    'skin-warning',
    'skin-primary',
    'skin-success',
    'skin-info-light',
    'skin-black-light',
    'skin-danger-light',
    'skin-warning-light',
    'skin-primary-light',
    'skin-success-light'
  ]

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== 'undefined') {
      return localStorage.getItem(name)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== 'undefined') {
      localStorage.setItem(name, val)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Toggles layout classes
   *
   * @param String cls the layout class to toggle
   * @returns void
   */
  function changeLayout(cls) {
    $('body').toggleClass(cls)
    if ($('body').hasClass('fixed') && cls == 'fixed') {
      $pushMenu.expandOnHover()
      $layout.activate()
    }
    $controlSidebar.fix()
  }

  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function changeSkin(cls) {
    $.each(mySkins, function (i) {
      $('body').removeClass(mySkins[i])
    })

    $('body').addClass(cls)
    store('skin', cls)
    return false
  }

  /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin')
    if (tmp && $.inArray(tmp, mySkins))
      changeSkin(tmp)

    // Add the change skin listener
    $('[data-skin]').on('click', function (e) {
      if ($(this).hasClass('knob'))
        return
      e.preventDefault()
      changeSkin($(this).data('skin'))
    })

    // Add the layout manager
    $('[data-layout]').on('click', function () {
      changeLayout($(this).data('layout'))
    })

    $('[data-controlsidebar]').on('click', function () {
      changeLayout($(this).data('controlsidebar'))
      var slide = !$controlSidebar.options.slide

      $controlSidebar.options.slide = slide
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open')
    })

    $('[data-sidebarskin="toggle"]').on('click', function () {
      var $sidebar = $('.control-sidebar')
      if ($sidebar.hasClass('control-sidebar-dark')) {
        $sidebar.removeClass('control-sidebar-dark')
        $sidebar.addClass('control-sidebar-light')
      } else {
        $sidebar.removeClass('control-sidebar-light')
        $sidebar.addClass('control-sidebar-dark')
      }
    })

    $('[data-enable="expandOnHover"]').on('click', function () {
      $(this).attr('disabled', true)
      $pushMenu.expandOnHover()
      if (!$('body').hasClass('sidebar-collapse'))
        $('[data-layout="sidebar-collapse"]').click()
    })

    $('[data-enable="rtl"]').on('click', function () {
      $(this).attr('disabled', true)
      $pushMenu.expandOnHover()
      if (!$('body').hasClass('rtl'))
        $('[data-layout="rtl"]').click()
    })

    $('[data-enable="dark"]').on('click', function () {
      $(this).attr('disabled', true)
      $pushMenu.expandOnHover()
      if (!$('body').hasClass('dark'))
        $('[data-layout="dark"]').click()
    })

    //  Reset options
    if ($('body').hasClass('fixed')) {
      $('[data-layout="fixed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('layout-boxed')) {
      $('[data-layout="layout-boxed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('sidebar-collapse')) {
      $('[data-layout="sidebar-collapse"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('rtl')) {
      $('[data-layout="rtl"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('dark')) {
      $('[data-layout="dark"]').attr('checked', 'checked')
    }

  }

  // Create the new tab
  var $tabPane = $('<div />', {
    'id'   : 'control-sidebar-theme-demo-options-tab',
    'class': 'tab-pane active'
  })

  // Create the tab button
  var $tabButton = $('<li />', { 'class': 'nav-item' })
    .html('<a href=\'#control-sidebar-theme-demo-options-tab\' class=\'active\' data-toggle=\'tab\'>'
      + 'Settings'
      + '</a>')

  // Add the tab button to the right sidebar tabs
  $('[href="#control-sidebar-home-tab"]')
    .parent()
    .before($tabButton)

  // Create the menu
  var $demoSettings = $('<div />')

  // Layout options
  $demoSettings.append(
    '<h4 class="control-sidebar-heading">'
    + 'Layout Options'
    + '</h4>'
    // Fixed layout
	+ '<div class="flexbox mb-15">'
	+ '<label for="layout_fixed" class="control-sidebar-subheading">'
    + 'Fixed layout'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-layout="fixed" id="layout_fixed">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'	
	  
    // Boxed layout
	+ '<div class="flexbox mb-15">'
	+ '<label for="layout_boxed" class="control-sidebar-subheading">'
    + 'Boxed Layout'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-layout="layout-boxed" id="layout_boxed">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'
	  
    // Sidebar Toggle
	+ '<div class="flexbox mb-15">'
	+ '<label for="toggle_sidebar" class="control-sidebar-subheading">'
    + 'Toggle Sidebar'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-layout="sidebar-collapse" id="toggle_sidebar">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'	  
    
    // Control Sidebar Toggle
	+ '<div class="flexbox mb-15">'
	+ '<label for="toggle_right_sidebar" class="control-sidebar-subheading">'
    + 'Toggle Right Sidebar Slide'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-controlsidebar="control-sidebar-open" id="toggle_right_sidebar">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'	  
	  	
    // Control Sidebar Skin Toggle
	+ '<div class="flexbox mb-15">'
	+ '<label for="toggle_right_sidebar_skin" class="control-sidebar-subheading">'
    + 'Toggle Right Sidebar Skin'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-sidebarskin="toggle" id="toggle_right_sidebar_skin">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'
	  
    // rtl layout
	+ '<div class="flexbox mb-15">'
	+ '<label for="rtl" class="control-sidebar-subheading">'
    + 'rtl'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-layout="rtl" id="rtl">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'
	  
    // dark layout
	+ '<div class="flexbox mb-15">'
	+ '<label for="dark" class="control-sidebar-subheading">'
    + 'Dark Layout'
    + '</label>'
	+ '<label class="switch switch-border switch-danger">'
	+ '<input type="checkbox" data-layout="dark" id="dark">'
	+ '<span class="switch-indicator"></span>'
	+ '<span class="switch-description"></span>'
	+ '</label>'
	+ '</div>'
  )
  
  var $skinsList = $('<ul />', { 'class': 'list-unstyled clearfix' })

  // Dark sidebar skins
  var $skinInfo =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-info" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span class="bg-info" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinInfo)
  var $skinBlack =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div class="clearfix"><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span style="display:block; width: 70%; float: left; height: 60px; background: #f4f6f9;"></span></div>'
            + '</a>')
  $skinsList.append($skinBlack)
  var $skinPrimary =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-primary" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span class="bg-primary" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinPrimary)
  var $skinSuccess =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-success" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span class="bg-success" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinSuccess)
  var $skinDanger =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-danger" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span class="bg-danger" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinDanger)
  var $skinWarning =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-warning" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #242a33;"></span><span class="bg-warning" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinWarning)

  // Light sidebar skins
  var $skinInfoLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-info-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span class="bg-info" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinInfoLight)
  var $skinBlackLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div class="clearfix"><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span style="display:block; width: 70%; float: left; height: 60px; background: #2A3E52;"></span></div>'
            + '</a>')
  $skinsList.append($skinBlackLight)
  var $skinPrimaryLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-primary-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span class="bg-primary" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinPrimaryLight)
  var $skinSuccessLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-success-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span class="bg-success" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinSuccessLight)
  var $skinDangerLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-danger-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span class="bg-danger" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinDangerLight)
  var $skinWarningLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px 20px 20px 0px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-warning-light" style="display: block;" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 30%; float: left; height: 60px; background: #f4f6f9;"></span><span class="bg-warning" style="display:block; width: 70%; float: left; height: 60px;"></span></div>'
            + '</a>')
  $skinsList.append($skinWarningLight)

  

  $demoSettings.append('<h4 class="control-sidebar-heading">Skins</h4>')
  $demoSettings.append($skinsList)

  $tabPane.append($demoSettings)
  $('#control-sidebar-home-tab').after($tabPane)

  setup()

  $('[data-toggle="tooltip"]').tooltip()
});// End of use strict





function openit(){
      document.getElementById("dl").style.pointerEvents = 'none';
      document.getElementById("anl").style.pointerEvents = 'none';
      document.getElementById("dc_check").style.pointerEvents = 'none';
    }

function enable_tab(ele){
  var name="";
   name = ele.getAttribute("name");
  console.log(name);
    if(name == "design_check")
  {
      document.getElementById("dc_check").style.pointerEvents = 'visible';
      document.getElementById("dl").style.pointerEvents = 'visible';
      document.getElementById("up").style.pointerEvents = 'none';
      document.getElementById("anl").style.pointerEvents = 'none';
    }
    else if(name == "upload")
    {
       document.getElementById("dc_check").style.pointerEvents = 'visible';
      document.getElementById("dl").style.pointerEvents = 'none';
      document.getElementById("anl").style.pointerEvents = 'none';
    }
    else if(name == "dl")
    {
      document.getElementById("up").style.pointerEvents = 'none';
       document.getElementById("dc_check").style.pointerEvents = 'none';
       document.getElementById("dl").style.pointerEvents = 'visible';
      document.getElementById("anl").style.pointerEvents = 'visible';

    }
}
var projectDataList;
var appendDynamicContent;
function dash()
{
$.ajax({
        type: "GET",
        url : "http://192.168.0.127:8000/project/get/",
        dataType: 'json',
        headers: {
            "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
        },
        success: function (response){
        console.log(response);
        projectDataList=response;
        renderDashboard(projectDataList);
        }
        });
}

 function conversiontoimg(con){

                      var item_image=con;
                      var src = "data:image/jpeg;base64,";
                      src += item_image;
                      var newImage = document.createElement('img');
                      newImage.src = src;
                      
                      console.log(newImage);
      /*document.querySelector('#imageContainer').innerHTML = newImage.outerHTML;*/
                       return newImage.outerHTML;

                       }

function renderDashboard(dataList){

for (i=0;i<dataList.length;i++){

appendDynamicContent="";
appendDynamicContent+="<div class='col-12 col-lg-3 pull-up'  onclick='openftp(this)' name='"+dataList[i]["name"]+"'>";
appendDynamicContent+="<div class='box'>";
appendDynamicContent+="<div class='box-header'>";
appendDynamicContent+="<h4 class='box-title''>"+dataList[i]["name"]+"</h4>";
appendDynamicContent+="<div class='pull-right d-block text-dark flexbox'>";
appendDynamicContent+="<a  data-toggle='modal' data-container='body' title='' data-target='#exampleModalCenter'>";
appendDynamicContent+="<i class='fa fa-edit' onclick='getEditData(this)' name='"+dataList[i]["name"]+"' data-toggle='tooltip' title='edit'></i>";
appendDynamicContent+="</a>";
appendDynamicContent+="<a  data-toggle='tooltip' data-container='body' title='' onclick='deleteproject(this)' name='"+dataList[i]["name"]+"'  data-original-title='Remove'>";
appendDynamicContent+="<i class='fa fa-trash-o'></i>";
appendDynamicContent+="</a>";
appendDynamicContent+="<a  data-toggle='modal' data-container='body'  data-target=' #largeModal' onclick='viewdetail(this)' name='"+dataList[i]["name"]+"'>";
appendDynamicContent +="<i class='fa fa-info-circle' data-toggle='tooltip' title='View'></i>";
appendDynamicContent+="</a>";
appendDynamicContent+="<a  data-toggle='modal' data-container='body'  data-target=' #modal' onclick='date_create(this)' name='"+dataList[i]["name"]+"'>";
appendDynamicContent +="<i class='fa fa-calendar' data-toggle='tooltip' title='Create Project Date'></i>";
appendDynamicContent+="</a>";
appendDynamicContent+="</div>";
appendDynamicContent+="</div>";
appendDynamicContent+="<a href='pages/product-detail-dashboard.html'>"+conversiontoimg(dataList[i]["image"])+"</a>";
appendDynamicContent+="<div class='box-body'>";
appendDynamicContent+="<div class='text-center my-2'>";
appendDynamicContent+="<div class='font-size-20'>"+dataList[i]["type_of_solution"]+"</div>";
appendDynamicContent+="<!-- <span>Tasks</span> -->";
appendDynamicContent+="</div>";
appendDynamicContent+="</div>";
appendDynamicContent+="<div class='box-body bg-gray-light py-20 text-center'>";
appendDynamicContent+="<span class='text-muted mr-1'>Created:</span>";
appendDynamicContent+="<span class='text-dark'>March 20, 2018  "+ (dataList[i]["state"]).toUpperCase()+"  ,"+(dataList[i]["country"]).toUpperCase()+"</span>";
appendDynamicContent+="</div>";
appendDynamicContent+="</div>";
appendDynamicContent+="</div>";

$('#dynamic_cards').append(appendDynamicContent);

//type_of_solution
}
}
var getname;
var my_response;
var items = {};

function getEditData(e)
{
  getname  = e.getAttribute("name");
  console.log(getname);
  $.ajax
  ({
    type: "GET",
    url: "http://192.168.0.127:8000/project/get/data/"+getname,
    dataType: 'json',
      headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    success: function (res){
      console.log(res);
      my_response=res;
      renderformdata();
       }
  });
  items = {};

}
var render_view;
var view_data;
var getview;
var img_view;

function viewdetail(ele)
{
  getview = ele.getAttribute("name");
  
   $.ajax
  ({
    type: "GET",
    url: "http://192.168.0.127:8000/project/get/data/"+getview,
    dataType: 'json',
      headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    success: function (res){
      console.log(res);
      view_data=res;
      $("#view_image").empty();
      img_view="";
      img_view += conversiontoimg(view_data[getview]["image"]);
      $("#view_image").append(img_view);
      document.getElementById("view_name").innerHTML = view_data[getview]["name"];
      document.getElementById("view_description").innerHTML = view_data[getview]["description"];
      $("#view_table").empty();
      render_view="";
      for(var key in view_data[getview])
        {
          if(key != "name" && key != "description" && key != "properties" && key != "image" && key != "data")
          {
          render_view += '<tr>';
          render_view +=  '<th style="text-transform: capitalize;">'+key+'</th>';
          render_view += '<td style="text-transform: capitalize;">'+view_data[getview][key]+'</td>';
          render_view += '</tr>';
        }
          
        }
        $("#view_table").append(render_view);
      
       }
  });

  
  
 

}

 
function renderformdata()
{
  document.getElementById("name").value = my_response[getname]["name"];
  document.getElementById("description").value = my_response[getname]["description"];
  document.getElementById("location").value = my_response[getname]["location"];
  document.getElementById("bound").value = my_response[getname]["boundary_coords"];
  document.getElementById("city").value = my_response[getname]["city"];
  document.getElementById("state").value = my_response[getname]["state"];
  document.getElementById("country").value = my_response[getname]["country"];
  document.getElementById("cal").value = my_response[getname]["start_date"];
  document.getElementById("duration").text = my_response[getname]["duration"];
}



function generatechanges()
{
  if(document.getElementById("name").value != my_response[getname]["name"])
  {
    items['name'] = document.getElementById("name").value;
  }
  if(document.getElementById("description").value != my_response[getname]["description"])
  {
    items['description'] = document.getElementById("description").value;
  }
  if(document.getElementById("location").value != my_response[getname]["location"])
  {
    items['location'] = document.getElementById("location").value;
  }
  if(document.getElementById("bound").value != my_response[getname]["boundary_coords"])
  {
    items['boundary_coords'] = document.getElementById("bound").value;
  }
  if(document.getElementById("city").value != my_response[getname]["city"])
  {
    items['city'] = document.getElementById("city").value;
  }
  if(document.getElementById("state").value != my_response[getname]["state"])
  {
    items['state'] = document.getElementById("state").value;
  }
  if(document.getElementById("country").value != my_response[getname]["country"])
  {
    items['country'] = document.getElementById("country").value;
  }
  if(document.getElementById("cal").value != my_response[getname]["start_date"])
  {
    items['start_date'] = document.getElementById("cal").value;
  }
  if(document.getElementById("duration").text != my_response[getname]["duration"])
  {
    items['duration'] = document.getElementById("duration").text;
  }
  console.log(items);



   $.ajax
  ({
    type: "PUT",
    url: "http://192.168.0.127:8000/project/update/"+getname,
    contentType: "application/json",
    data:JSON.stringify(items),
    dataType: "json",
    headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    success: function(){
      alert("changes successfull");
      $("#dynamic_cards").empty();
      dash();
    },
    error: function(e) {
      console.log(e);
    }
  });
}

function deleteproject(elm)
{
  var getprojectname  = elm.getAttribute("name");
  $.ajax
  ({
    type: "PUT",
    url: "http://192.168.0.127:8000/project/delete/"+getprojectname,
    
    headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    success: function(){
      alert("deleted successfull");
      $("#dynamic_cards").empty();
      dash();
    },
    
  });
}



var send_date;
var create_name;
function date_create(ele)
{
   create_name = ele.getAttribute("name");
  // $("#create_date").empty();
  // send_date = "";
  // send_date +='<input type="text" class="form-control" data-toggle="datepicker" placeholder="Select Date">';
  // $("#create_date").append(send_date);
}
var date_item; 
function submit_date()
{
  var format = document.getElementById("date_select").value;
  console.log(format);
  var datearray = format.split("/");
  var newdate = datearray[2] + '-' + datearray[0] + '-' + datearray[1];
  console.log(newdate);
  date_item = {};
  date_item["date"] = newdate;
  console.log(date_item);

   $.ajax
  ({
    type: "POST",
    url: "http://192.168.0.127:8000/project/create/date/"+create_name,
    
    headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    contentType: "application/json",
    data:JSON.stringify(date_item),
    dataType: "json",
    success: function(res){
      
      console.log(res);
      
    },
    
  });
}

function openftp(elm)
{
  var setname = elm.getAttribute("name");
  localStorage.setItem("project_name", setname);
  var project_name = localStorage.getItem("project_name");
  console.log(project_name);
  
}

var ftpresp;
var ftpdropdown="";
var project_name;

  project_name = localStorage.getItem("project_name");
  console.log(project_name);
  // rendereddate(); 
  




   $.ajax
  ({
    type: "GET",
    url: "http://192.168.0.127:8000/project/get/dates/"+project_name,
    dataType: 'json',
      headers: {
      "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
    },
    success: function (res){
      ftpresp = res;
      console.log(ftpresp);
      
      var v = 0;
      for(var key in ftpresp["dates"])
      {
      ftpdropdown += '<option value="'+ftpresp["dates"][key]+'">'+ftpresp["dates"][key]+'</option>';
      
      
      }
      $("#select_date").append(ftpdropdown);

      
    },
  });

function changeFunc(){
  var selectBox = document.getElementById("select_date");
  var selectedValue = selectBox.options[selectBox.selectedIndex].value;
  console.log(selectedValue);

  localStorage.setItem("ftp_date",selectedValue);
}





  var editForm="";

editForm+="<div class='modal-dialog modal-dialog-centered' role='document'>";
editForm+="<div class='modal-content'>";
editForm+="<div class='modal-header'>";
editForm+="<h4 class='modal-title' id='exampleModalLongTitle'>Edit Project</h4>";
editForm+="<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
editForm+="<span aria-hidden='true'>&times;</span>";
editForm+="</button>";
editForm+="</div>";
editForm+="<div class='modal-body'>";
editForm+="<div class='box'>";
editForm+="<!-- /.box-header -->";
editForm+="<div class='box-body'>";
editForm+="<div class='row'>";
editForm+="<div class='col-md-12'>";
editForm+="<div class='form-group'>";
editForm+="<label>Name</label>";
editForm+="<input type='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='Name' id='name'>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>Description</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' id='description' class='form-control' required='' placeholder='Description' aria-invalid='false' >";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>Location</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' name='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='Location' id='location'>";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>Upload Image</label>";
editForm+="<div class='input-group'>";
editForm+="<span class='input-group-btn'>";
editForm+="<span class='btn btn-default btn-file'>";
editForm+="Browse… <input type='file' id='imgInp'>";
editForm+="</span>";
editForm+="</span>";
editForm+="<input type='text' class='form-control' >";
editForm+="</div>";
editForm+="<img id='img-upload'>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>Boundary Coordinates</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' name='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='Boundary Coordinates' id='bound'  >";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>City</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' id='city' name='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='City'  > <div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>State</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' id='state' name='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='State' > <div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='form-group'>";
editForm+="<label>Country</label>";
editForm+="<div class='controls'>";
editForm+="<input type='text' id='country' name='text' class='form-control' required='' data-validation-required-message='This field is required' aria-invalid='false' placeholder='Country' > <div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='row'>";
editForm+="<div class='col-md-12'>";
editForm+="<div class='form-group' id='monitor'>";
editForm+="<label>Frequency of Monitoring</label>";
editForm+="<p>(Only for ICPM, SCPM, AFHM, AMP)</p>";
editForm+="<div class='controls'>";
editForm+="<select name='select' id='select' required='' class='form-control' aria-invalid='false'>";
editForm+="<option value='' >Select</option>";
editForm+="<option value='1'>Weekly</option>";
editForm+="<option value='2'>Fortnightly</option>";
editForm+="<option value='3'>Monthly</option>";
editForm+="<option value='4'>Custom</option>";
editForm+="</select>";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='row'>";
editForm+="<div class='col-md-12'>";
editForm+="<div class='form-group'>";
editForm+="<label>Project Start Date</label>";
editForm+="<div id='datepickererwer' class='input-groupree dateewrw' data-date-format='mm-dd-yyyy'>";
editForm+="<input type='date' id='cal' >";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='row'>";
editForm+="<div class='col-md-12'>";
editForm+="<div class='form-group'>";
editForm+="<label>Duration</label>";
editForm+="<div class='controls'>";
editForm+="<select name='select' id='select' required='' class='form-control' aria-invalid='false'>";
editForm+="<option value='' id='duration'></option>";
editForm+="<option value='1'>Days</option>";
editForm+="<option value='2'>Weeks</option>";
editForm+="<option value='3'>Months</option>";
editForm+="</select>";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="<div class='row'>";
editForm+="<div class='col-md-12'>";
editForm+="<div class='form-group' id='crop-hide'>";
editForm+="<label>Crop</label>";
editForm+="<p>(Only for Agri)</p>";
editForm+="<div class='controls'>";
editForm+="<select name='select' id='select' required='' class='form-control' aria-invalid='false'>";
editForm+="<option value=''>Select</option>";
editForm+="<option value='1'>Corn</option>";
editForm+="<option value='2'>Rice</option>";
editForm+="</select>";
editForm+="<div class='help-block'></div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";

editForm+="<div class='modal-footer'>";
editForm+="<button type='button' class='btn btn-secondary' data-dismiss='modal' >Close</button>";
editForm+="<button type='button' class='btn btn-primary save-right' onclick='generatechanges()'>Save changes</button>";
editForm+="</div>";
editForm+="</div>";
editForm+="</div>";

editForm+="</div>";
editForm+="</div>";

$("#exampleModalCenter").append(editForm);



window.onload= dash();





