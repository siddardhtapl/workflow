$(".tab-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "none"
//    , titleTemplate: '<span class="step">#index#</span> #title#'
    , titleTemplate: '<span class="step"></span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onFinished: function (event, currentIndex) {
         //    alert("Hurrayyyy!! Submitted123654 ");

       swal("Your Order Submitted!", "Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor.");
            
    }
});


var form = $(".validation-wizard").show();

$(".validation-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "none"
    , titleTemplate: '<span class="step"></span> #title#'
//    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onStepChanging: function (event, currentIndex, newIndex) {
        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
    }
    , onFinishing: function (event, currentIndex) {
        console.log(currentIndex);
        return form.validate().settings.ignore = ":disabled", form.valid()
    }
    , onFinished: function (event, currentIndex) {
        var data={}
		data['name']=document.getElementById("project_name").value;
		data['sector']=$("#sector option:selected").text();
		data['type_of_solution']=$("#type_of_solution option:selected").text();
		data['description']=document.getElementById("project_description").value;
		data['location']=document.getElementById("project_location").value;
		data['boundary_coords']=document.getElementById("project_boundary").value;
		data['city']=document.getElementById("project_city").value;
		data['state']=document.getElementById("project_state").value;
		data['country']=document.getElementById("project_country").value;
		data['start_date']=changeDateFormat($("#start_date").val());
        if($("#type_of_solution option:selected").text()=="Construction Progress Monitoring (SCPM)" || $("#type_of_solution option:selected").text()=="Construction Progress Monitoring (ICPM)" || $("#type_of_solution option:selected").text()=="Farm Health Management (AFHM)")
		data['frequency']=$("#project_monitor option:selected").text()
		data['duration']=$("#project_duration option:selected").text();
		data['image']=base;
		if($("#sector option:selected").text()=="Agriculture")
		data['crop']=$("#project_crop_type option:selected").text()
        console.log(data);
        $.ajax({
		 url:"http://192.168.0.127:8000/project/create/",
		 type:"POST",
		 headers: {
            "Authorization": 'token d6f68e3d00c884b534be6f525ca21c7f3d7027a1',
         },
		 contentType:'application/json',
		 data:JSON.stringify(data),
		 dataType: "json",
		 success: function(respData){
		 console.log(respData);
		 if(respData){
		 swal("Project Created Successfully!");
			location.replace("../index.html")
		}},
		 error: function(e) {
		   console.log(e);
		   alert (e);
		 }
		});
    }
}), $(".validation-wizard").validate({
    ignore: "input[type=hidden]"
    , errorClass: "text-danger"
    , successClass: "text-success"
    , highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , errorPlacement: function (error, element) {
        error.insertAfter(element)
    }
    , rules: {
        email: {
            email: !0
        }
    }
})

 var base="";
 function encodeImagetoBase64(element) {
    //console.log(element)
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
        base=reader.result.split(',')[1];
        //console.log(base);
        }

    reader.readAsDataURL(file);

  }

 function changeDateFormat(date){
var  date=date.split("-");
var  changedDate=date[2]+"-"+date[0]+"-"+date[1];
return changedDate;
 }

